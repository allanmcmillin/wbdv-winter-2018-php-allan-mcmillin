<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'FrontController@view' );

Route::post('/post', 'PostController@create' );
Route::get('/posts/{id}', 'PostController@userPosts');
Route::get('/posts/{id}/like/toggle', 'PostController@toggleLike');

Route::post('/comment', 'CommentController@create');
// View user content
Route::get('/user', 'UserController@view');
Route::post('/user', 'UserController@create' );

Auth::routes();

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();
Route::get('/contact', 'ContactController@create');
Route::post('/contact', 'ContactController@store');

// Route::get('/home', 'HomeController@index')->name('home');
