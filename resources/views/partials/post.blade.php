<a href="/posts/{{$post->user->id}}">
        <h5>
            {{$post->user->name}}
        </h5>
        {{$post->content}}
        <br>
</a>
<div class="container bg-primary text-light">
    <form action="/comment" method="post">
        <?php echo csrf_field()?>
        <textarea type="text" name="commentContent" rows='1' cols='80'
            placeholder="Comment"
            class="{{ $errors->has('commentContent') ? 'alert-danger': '' }}"
            >{{ old('commentContent') }}</textarea>
        <br>
        <input type='hidden' name='post_id' value="{{$post->id}}">
        <input type="submit"  value="Comment">
    </form>
    Likes: {{count($post->likes)}}
    <br>
    
    @if ($post->isLikedByCurrentUser())
        <a href="/posts/{{ $post->id }}/like/toggle">Unlike</a>
    @else
        <a href="/posts/{{ $post->id }}/like/toggle">Like</a>
    @endif

    <ul>
        @foreach ($comments as $comment)
            @if($comment->post_id ==$post->id)
            <li>
                Comment: {{$comment->content}} <br>
                User:    {{$comment->user_id}} <br>
                Time:   {{$comment->created_at->diffForHumans()}}
            </li>
            @endif
        @endforeach

    </ul>
