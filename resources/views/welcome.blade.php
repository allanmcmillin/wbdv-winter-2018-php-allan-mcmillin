@extends('layout')
@section('content')
    <body>
        <div class="container bg-primary text-light">
            <h1>Post Now</h1>
            <form action="/post" method="post">
                <?php echo csrf_field()?>
                <textarea type="text" name="newContent" rows='3' cols='80'
                    placeholder="New Content"
                    class="{{ $errors->has('newContent') ? 'alert-danger': '' }}"
                    >{{ old('newContent') }}</textarea>
                <br>
                <input type="submit" name="" value="Create Post">
            </form>
            <h2>Users</h2>

            <br>
            <ol>
                @foreach ($users as $user)
                    <li>
                        @include('partials.user')
                    </li>
                @endforeach
            </ol>
            <h2>Posts</h2>
                <ul>
                     @foreach ($posts as $post)
                        <li>
                            @include('partials.post')
                        </li>
                    @endforeach
                </ul>
        </div>

    </body>
</html>
@endsection
