<input
    type="text"
    name="{{ $name }}"
    placeholder="{{ $label }}"
    value="{{ old( $name ) }}"
    class="{{ $errors->has( $name ) ? 'alert-danger': '' }}"
>
