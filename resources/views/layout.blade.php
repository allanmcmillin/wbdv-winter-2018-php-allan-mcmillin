<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>SAIT CPNT 262 Final Assignment</title>
        <link rel="stylesheet" href="/css/app.css">
        <link rel="stylesheet" href="/css/master.css">
    </head>
    <body>
        <div class="login-links">

            @if (Auth::check())
                    Hello, {{Auth::user()->name}}
                    <a href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                                     document.getElementById('logout-form').submit();">
                        Logout
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                    <a href="/contact">Contact</a>
                @else
                    <a href="/login">Login</a>
                    <a href="/register">Register</a>
                    <a href="/">Home</a>
                    <a href="/contact">Contact</a>
                    <style>
                        form {
                            display: none;
                        }
                    </style>
                @endif

        </div>
        <?php if($message = session('message')): ?>
                <div class="alert alert-success">
                    <?php echo $message ?>
                </div>
        <?php endif ?>
        <?php if($errors->any()): ?>
            <div class="alert alert-danger">
                <ul>
                    <?php foreach ($errors->all() as $error): ?>
                        <li>
                            <?php echo $error ?>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        <?php endif ?>
        <a id="headline-0" href="/" >
            <h1 id="headline-1">
                SAIT CPNT 262 Final Assignment
            </h1>
        </a>
        @yield('content')
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <script type="text/javascript" scr="/js/app.js">

        </script>
    </body>
</html>
