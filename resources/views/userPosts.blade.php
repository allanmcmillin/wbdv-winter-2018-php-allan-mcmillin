@extends('layout')

@section('content')
    <div class="container">
        <h1>{{ $user->name }}</h1>
        <ul>
            @foreach ($posts as $post)
                <li>
                    {{ $post->content }}
                    {{-- {{$comments->content}} --}}
                </li>
            @endforeach
        </ul>
    </div>
@endsection
