@extends('layout')

@section('content')


    <h1>Contact Form</h1>

    <form id="contactform" method="post">
        <?php echo csrf_field()?>
        @include('forms.text', [
            'label' => 'Name',
            'name' => 'name'
        ])
        <br>
        @include('forms.text', [
            'label' => 'Email',
            'name' => 'email'
        ])

        <br>

        <textarea
            type="text"
            name="message"
            placeholder="Message"
            rows="5"
            cols="80"
            class="<?php echo $errors->has('message') ? 'alert-danger': '' ?>"
            ><?php echo old('message') ?></textarea>
        <br>
        <input type="submit" name="" value="Submit">
    </form>

@endsection
