<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Contact;

class ContactController extends Controller
{
    public function create(){
        return view('contact');
    }
    public function store(){
        // Form Validation
        $request = request();
        $result = $request->validate([
            'name' => 'required|max:255',
            'email' => 'required|max:255'
        ]);

        $result = $request->validate([
            'message' => 'required|max:255'
        ]);
        // Create variables for the form
        $data = request()->all();
        $contact = new Contact();
        $contact->name = $data['name'];
        $contact->message = $data['message'];
        $contact->email = $data['email'];
        $contact->save();
        return redirect('/contact')->with('message','Your contact was created succuessuflly');
    }
}
