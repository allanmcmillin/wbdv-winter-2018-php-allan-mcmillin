<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\Comment;
use App\User;

class PostController extends Controller
{
    public function create(){
        if (!\Auth::check()) {
            return redirect('/login');
        }
        // form Validation
        $request = request();
        $result = $request->validate([
            'newContent' => 'required|max:255'
        ]);
        // do form processing here
        $data = request()->all();
        $posts = new Post();
        $loggedInUser = $request->user();
        $posts->user_id = $loggedInUser->id;
        $posts->content = $data['newContent'];
        $posts->save();
        return redirect('/')->with('message','Your post was posted succuessuflly');
    }
    public function userposts($userId)
    {
        $user = User::where('name', $userId)
            ->orWhere('id', $userId)
            ->first();
        if(!$user){
            abort(404);
        }
        $posts= $user->posts;
        return view('userposts',[
            'user' => $user,
            'posts' => $posts

        ]);
    }
    public function toggleLike($postId){
        if (!\Auth::check()) {
            return redirect('/login');
        }
        $user = request()->user();
        $post = Post::find($postId);
        if ($post->isLikedByCurrentUser()){
            $post->likes()->detach($user);
        } else {
            $post->likes()->attach($user);
        }
        return back()->with('message','You sucessfully liked or unliked a post');
    }
}
