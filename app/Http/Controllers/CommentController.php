<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Comment;
use App\User;

class CommentController extends Controller
{
    public function create(){
        if (!\Auth::check()) {
            return redirect('/login');
        }
        // form Validation
        $request = request();
        $result = $request->validate([
            'commentContent' => 'required|max:255'
        ]);

        // do form processing here
        $data = request()->all();
        $comment = new Comment();
        $loggedInUser = $request->user();
        $comment->user_id = $loggedInUser->id;
        $comment->content = $data['commentContent'];
        $comment->post_id = $data['post_id'];
        $comment->save();
        return redirect('/')->with('message','Your comment was posted succuessuflly');
    }
}
