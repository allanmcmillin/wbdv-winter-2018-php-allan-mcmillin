<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Post;
use App\Models\Comment;
use App\User;

class FrontController extends Controller
{
    public function view() {
        // Loading the posts from the database
        $posts = Post::all();
        // Loading Users from database
        $users = User::all();
        // Loading comments
        $comments = Comment::all();

        $data = [
            'users' => $users,
            'posts' => $posts,
            'comments' => $comments
        ];
        return view('welcome', $data);
    }
}
