<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Models\Comment;

class UserController extends Controller
{
    public function create(){
        // form Validation
        $request = request();
        $result = $request->validate([
            'name' => 'required|max:255',
            'email' => 'required|max:255',
            'password' => 'required|max:255'
        ]);
        $data = request()->all();
        $users = new User();
        $users->name = $data['name'];
        $users->email = $data['email'];
        $users->password = $data['password'];
        $users->save();
        return redirect('/')->with('message','Your user was created succuessuflly');
    }
    public function view(){

       return view('user');
   }
}
